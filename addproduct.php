<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title>Junior Developer Test</title>
        <link rel="shortcut icon" href="inc/logo.png">
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>        
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/d3js/6.2.0/d3.min.js"></script>
       
    </head>
    <body>
       
        
        <header class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-1 bg-white border-bottom shadow-sm fixed-top">
            <p class="h2 my-0 me-md-auto fw-normal text-danger"><b>Product Add</b></p>
            <?php
                require_once('db_connection.php');
                global $conn;
                $query="SELECT * from products";
                $result = mysqli_query($conn, $query);
                $rowcount=mysqli_num_rows($result); 

            ?>
            <nav class="navbar navbar-expand-lg navbar-light  ">
                <div class="container-fluid ">
                        <br>
                        
                        <!-- Button Validation to avoid entries more than!-->
                        <button class="btn btn-success " type="submit" 
                        <?php  
                            require_once('db_connection.php');
                            global $conn;
                            $query="SELECT * FROM products";
                            $result = mysqli_query($conn, $query);
                            $rowcount=mysqli_num_rows($result); 
                            if ($rowcount >= 120) { ?> disabled <?php   }  ?> 
                            form="product_form" ><b>Save</b></button> &nbsp;&nbsp;
                            
                        <!-- Cancel Button!-->
                        <button class="btn btn-outline-danger "  onclick="window.location.href='index.php'"><b>Cancel</b></button>&nbsp;&nbsp;
                       
                 </div>
            </nav>
            
        </header>
    <br><br><br><br><br><br>
    <main class="container " >

    <form id="product_form" method="post" action="product_form.php">
    <div class="form-group row">
        <label  class="col-sm-2 col-form-label"><b>SKU</b></label>
        <div class="col-sm-3">
        <input type="text" class="form-control mb-4" id="sku" name="sku" placeholder="SKU" required onkeydown="return  event.keyCode !== 32" onpaste="return false;">
        </div>
    </div>
    
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"><b>Name</b></label>
        <div class="col-sm-3">
        <input type="text" class="form-control mb-4" id="name" name="name" placeholder="Name" required>
        </div>
    </div>

    <div class="form-group row">
        <label  class="col-sm-2 col-form-label"><b>Price ($)</b></label>
        <div class="col-sm-3">
        
        <input type="number" class="form-control mb-4 price" id="price" name="price" placeholder="Price ($)" required  min="1" step="1" onpaste="return false;" onkeydown="return  event.keyCode !== 69 && event.keyCode !== 187 && event.keyCode !== 189"">
        <script type="text/javascript">
        $(document).ready(function () {
        $(".price").change(function() {
            $(this).val(parseFloat($(this).val()).toFixed(2));
               });
            });
        </script>
        </div>
    </div>

    <div class="form-group row">
        <label  class="col-sm-2 col-form-label"><b>Type Switcher</b></label>
        <div class="col-sm-4 pt-1">
            
            <select class="form-group" id = "productType" name="type" required >
                <option disabled selected value><b>Type Switcher</b></option>
                <option value="Size" id ="DVD" name="DVD"><b> DVD </b></option>
                <option value="Dimension" id="Furniture" name="Furniture"><b> Furniture </b></option>
                <option value="Weight" id="Book" name="Book"><b> Book </b></option>
            </select>
        </div>
    </div>

    <script>
        $(document).ready(function(){ 
            $("select").change(function(){
                $(this).find("option:selected").each(function(){
                    var optionValue = $(this).attr("value");
                    if(optionValue){ 
                        $(".box").not("." + optionValue).hide();
                        $("." + optionValue).show();
                        $(".rem").not("." + optionValue).removeAttr('required').val(""); //removes required attribute from the feilds which are hidden
                    }
                    else{
                        $(".box").hide();
                    }
                });
            }).change();
        });
        </script>
    <br>

    <div class="form-group row Size box" id="DVD">
        <label  class="col-sm-2 col-form-label"><b>Size (MB)</b></label>
        <div class="col-sm-3">
        <input type="number" class="form-control mb-4 Size rem" id="size" name="size" placeholder="Size in MB" required min="1" step="1" onpaste="return false;" onkeydown="return event.keyCode !== 69 && event.keyCode !== 187 && event.keyCode !== 189"">
        <center><small><i>**Please provide size format**</i></small></center>  
        </div>
    </div>
  
    <div class="form-group row Dimension box" id="Furniture">
        <label  class="col-sm-2 col-form-label"><b>Height (CM)</b></label>
        <div class="col-sm-3">
        <input type="number" class="form-control mb-4 Dimension rem" id="height" name="height" placeholder="Height in (CM)" required min="1" step="1" onpaste="return false;" onkeydown="return event.keyCode !== 69 && event.keyCode !== 187 && event.keyCode !== 189"">
        </div>
    </div>
    <div class="form-group row Dimension box" id="Furniture">
        <label  class="col-sm-2 col-form-label"><b>Width (CM)</b></label>
        <div class="col-sm-3">
        <input type="number" class="form-control mb-4 Dimension rem" id="width" name="width" placeholder="Width in (CM)" required min="1" step="1" onpaste="return false;" onkeydown="return event.keyCode !== 69 && event.keyCode !== 187 && event.keyCode !== 189"">
        </div>
    </div>
    <div class="form-group row Dimension box" id="Furniture">
        <label  class="col-sm-2 col-form-label"><b>Length (CM)</b></label>
        <div class="col-sm-3">
        <input type="number" class="form-control mb-4 Dimension rem" id="length" name="length" placeholder="Length in (CM)" required min="1" step="1" onpaste="return false;" onkeydown="return event.keyCode !== 69 && event.keyCode !== 187 && event.keyCode !== 189">
        <center><small><i>**Please provide dimensions**</i></small></center>
        </div>
    </div>
    <div class="form-group row Weight box" id="Book">
        <label  class="col-sm-2 col-form-label"><b>Weight (KG)</b></label>
        <div class="col-sm-3">
        <input type="number" class="form-control mb-4 Weight rem" id="weight" name="weight" placeholder="Weight in (KG)" required min="1" step="1"  onpaste="return false;" onkeydown="return event.keyCode !== 69 && event.keyCode !== 187 && event.keyCode !== 189">
        <center><small><i>**Please provide weight format**</i></small></center>
        </div>
    </div>

   
  
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script>
	
			//if type is size
			$("#size").on("keyup", function(){
				$("#typev").val($("#size").val() + " MB");    
			});

			//if type is dimentions
			$("#height, #width , #length").on("keyup", function(){
				$("#typev").val($("#height").val() + "x" + $("#width").val()+ "x" + $("#length").val());
			});

			//if type is weight
			$("#weight").on("keyup", function(){
				$("#typev").val($("#weight").val() + "KG" );
			});

	</script>


    <input type="text" class="form-control mb-4 " id="typev"   hidden name="typev"  >

	</div>  
    </form>
    </div>

    </<br>
            <footer class="pt-4 my-md-5 pt-md-4 border-top fixed-bottom " >
                <center><b>Scandiweb Test assignment</b></center>
            </footer>




        </main>
    </body>
    </html>